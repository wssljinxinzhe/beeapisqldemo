# BeeApiSqlDemo
##
[Beego開発環境Set up.](https://qiita.com/anegawa_j/items/887c47049025029eba5d)
##

## Beego FrameWorkとBee ToolをGo Pathにinstall.
    go get -u github.com/beego/bee
    go get -u github.com/astaxie/beego
##
## Beego Api生成。(postgresqlの情報全部config fileに設定してくれます。)
##
	bee api psqldemo -tables="company" -driver=postgres -conn="user=xjin host=localhost dbname=beehive sslmode=disable"

##
## Databaseに接続するCodeを自動生成してくれます。
##
	bee generate appcode -driver=postgres -conn="postgres://xjin:@127.0.0.1:5432/beehive?sslmode=disable"
##
## BeegoApiを起動し、Swagger install.(Swaggerをinstallしないと、PostManとのやりとり、うまくいけないです。)
##
	bee run -gendoc=true -downdoc=true
##

![Get](img/get.png)
![項目確認](img/fieldnamecheck.png)
![Post新データを作成。](img/post.png)
![作ったものを確認](img/createddatacheck.png)
![log](img/log.png)
![確認](img/postmethod.png)